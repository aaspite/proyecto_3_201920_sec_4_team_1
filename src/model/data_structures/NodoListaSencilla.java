package model.data_structures;


public class NodoListaSencilla<E> 
{



	/**
	 * Elemento almacenado en el nodo.
	 */


	protected E elemento; 

	protected NodoListaSencilla<E> anterior;

	/**
	 * Siguiente nodo.
	 */

	protected NodoListaSencilla<E> siguiente; 

	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenar� en el nodo. elemento != null
	 */
	public NodoListaSencilla(E elemento)
	{


		this.elemento = elemento; 
	}

	public NodoListaSencilla(E element, NodoListaSencilla<E> next, NodoListaSencilla<E> prev)
	{
		elemento = element;
		siguiente = next;
		anterior = prev;

	}

	/**
	 * M�todo que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(NodoListaSencilla<E> siguiente)
	{


		this.siguiente = siguiente;
	}

	/**
	 * M�todo que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{


		return elemento;
	}

	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenar� en el nodo.
	 */
	public void cambiarElemento(E elemento)
	{


		this.elemento = elemento;
	}


	/** 
	 * M�todo que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public NodoListaSencilla<E> darSiguiente()
	{


		return siguiente;
	}

}
