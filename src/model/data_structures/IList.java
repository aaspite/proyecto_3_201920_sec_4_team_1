package model.data_structures;

public interface IList<E>  {
	/**
	 * agrega en primera posicion al item E
	 * @param item, item a agregar
	 */
	public void addFirst(E item);

	/**
	 * remueve el primer elemento
	 */
	public void removeFirst();

	/**
	 * agrega al final el item dado
	 * @param item, item a a agregar
	 */
	public void append(E item);

	/**
	 * remueve al elemento de la posicion dada
	 * @param pos, posicion a eliminar
	 */
	public void remove(int pos);

	/**
	 * retorna el elemento en dicha posicion
	 * @param pos, posicion solicitada
	 * @return elemetno en dicha posicion
	 */
	public E get (int pos);

	/**
	 * tama�o del arreglo
	 * @return tame�o del arreglo
	 */
	public int size();

	/**
	 * valida si esta vacio el arreglo
	 * @return true si si esta vacio
	 */
	public boolean isEmpty();

	/**
	 * remplaca un elemento en dicha posicion
	 * @param item a remplazar
	 * @param pos, posicion a remplazar
	 */
	public void replace(E item, int pos);
}
