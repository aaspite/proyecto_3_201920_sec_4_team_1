package model.data_structures;

public interface IStack<E>  extends Iterable<E>
{
	/**
	 * checks if the stack is empty
	 * @return true if is empty,false if isn�t
	 */
public boolean isEmpty();

/**
 * gives the size of the stack
 * @return a number according to the size of the stack
 */
public int size();

/**
 * inserts a item in the top of the Stack
 * @param item the new element to stack it up
 */
public void push(E item);

/**
 * removes the most recent element in the stack
 */
public void pop();
}
