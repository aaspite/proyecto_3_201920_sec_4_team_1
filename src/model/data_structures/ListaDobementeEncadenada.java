package model.data_structures;


import java.util.NoSuchElementException;


import java.util.Iterator;
/**
 * Clase que representa la lista doblemente encadenada
 * @param <E> Tipo de los objetos que almacenar� la lista.
 */
public class ListaDobementeEncadenada<E extends Comparable >  implements Iterable<E> 
{

	

	private NodoListaSencilla<E> head;
	private NodoListaSencilla<E> tail;
	private int size;

	public ListaDobementeEncadenada() 
	{
		
		size = 0; 
	}
	
	public NodoListaSencilla<E> getHead()
	{
		return head;
	}
	
	
	/**
	 * returns the size of the linked list
	 * @return
	 */
	public int size() { return size; }
	

	/**
	 * return whether the list is empty or not
	 * @return
	 */
	public boolean isEmpty() { return size == 0; }

	/**
	 * adds element at the starting of the linked list
	 * @param element
	 */
	public void addFirst(E element) {
		NodoListaSencilla<E> tmp = new NodoListaSencilla<E>(element, head, null);
		if(head != null ) 
		{
			head.anterior = tmp;
		}
		head = tmp;
		if(tail == null) 
		{ 
			tail = tmp;
		}
		size++;
	}

	/**
	 * adds element at the end of the linked list
	 * @param element
	 */
	public void addLast(E element) {

		NodoListaSencilla<E> tmp = new NodoListaSencilla<E>(element, null, tail);
		if(tail != null) {tail.siguiente = tmp;}
		tail = tmp;
		if(head == null) { head = tmp;}
		size++;
	}

	/**
	 * this method walks forward through the linked list
	 */
	public void iterateForward(){

		NodoListaSencilla<E> tmp = head;
		while(tmp != null){
			tmp = tmp.siguiente;
		}
	}

	/**
	 * this method walks backward through the linked list
	 */
	public void iterateBackward(){

		NodoListaSencilla<E> tmp = tail;
		while(tmp != null){
			tmp = tmp.anterior;
		}
	}

	/**
	 * this method removes element from the start of the linked list
	 * @return
	 */
	public E removeFirst() {
		
		if (size == 0) throw new NoSuchElementException();
		
		NodoListaSencilla<E> tmp = head;
		head = head.siguiente;
		if (head != null)
			head.anterior = null;
		size--;
		return tmp.elemento;
	}

	/**
	 * this method removes element from the end of the linked list
	 * @return
	 */
	public E removeLast() {
		if (size == 0) throw new NoSuchElementException();
		NodoListaSencilla<E> tmp = tail;
		tail = tail.anterior;
		if (tail != null)
			tail.siguiente = null;
		size--;
		return tmp.elemento;
	}
	
	public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        } else {
        	NodoListaSencilla<E> current = head;
            for (int i = 0; i < index; i++) {
                current = current.siguiente;
            }
            return current.elemento;
        }
    }
	
	public Iterator<E> iterator() 
	{
		return new  IteradorSencillo<E> (head);
	}
	
	public class IteradorSencillo< E extends Comparable > implements Iterator<E >
	{

		
		
		/**
		 * El nodo donde se encuentra el iterador.
		 */
		private NodoListaSencilla<E> actual;

		
		public IteradorSencillo(NodoListaSencilla<E> primerNodo) 
		{
			actual = primerNodo;
		}
		
		/**
	     * Indica si a�n hay elementos por recorrer
	     * @return true en caso de que  a�n haya elemetos o false en caso contrario
	     */
		public boolean hasNext() 
		{
			return actual != null;
		}

		/**
	     * Devuelve el siguiente elemento a recorrer
	     * <b>post:</b> se actualizado actual al siguiente del actual
	     * @return objeto en actual
	     * @throws NoSuchElementException Si se encuentra en el final de la lista y se pide el siguiente elemento.
	     */
		public E next() 
		{
			if(actual == null)
				throw new NoSuchElementException("Se ha alcanzado el final de la lista");
			E valor = actual.elemento;
			actual = actual.siguiente;
			return valor;
		}
		
		

	}
	
}
