package model.data_structures;

import java.util.Iterator;

public class Stack <E>implements IStack<E> {
	//---------
	//Atributos
	//---------
	
	/**
	 * referencia a la cabeza de la pila
	 */
	private Node<E> head;

	//---------
	//Metodos
	//---------
	
	/**
	 * contrustor de la pila
	 */
	public Stack() {
		head = null;
	}

	@Override
	public Iterator<E> iterator() {
		return new iterator<E>(head);

	}

	@Override
	public boolean isEmpty() {
		return head == null;
	}

	@Override
	public int size() {
		int count = 0;
		Node<E> p = head;
		while (p != null) {
			count++	;
			p = p.getNext();
		}
		return count;
	}

	@Override
	public void push(E t) {
		Node<E> p = head;

		Node<E> node = new Node<E>(t);
		head = node;
		node.setNext(p);
	}

	@Override
	public void pop() {
		// TODO Auto-generated method stub
		Node<E> list = head;
		head=head.getNext();
		list.setNext(null);
	}

	/**
	 * retorna la cabeza
	 * @return cabeza
	 */
	public Node<E> getHead()
	{
		return head;
	}



}

