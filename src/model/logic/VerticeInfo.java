package model.logic;

import java.util.Iterator;

public class VerticeInfo implements Comparable<VerticeInfo>{
	
	int id;
	double lat;
	double lon;
	int movementId;
	int[] adj;
	int adjActual;
	int adjSize;
	double velocidadPromedio;
	private int uniqueId;
	
	public VerticeInfo(int id, double lat,double lon,int movementId, int[] adj){
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.movementId = movementId;
		adjActual=0;
		this.velocidadPromedio = 0;
		this.adjSize = 0;
		this.adj=new int[50];
		
		
	}
	
	public int id(){
		return this.id;
	}
	
	public int[] adj(){
		return this.adj;
	}
	
	public void addAdj(int id){
		
		//expandir arreglo
		if(adj.length == adjSize){
			int[] expanded = new int[adj.length + 50];
			for(int i=0; i<adj.length;i++)
			{
				expanded[i] = adj[i];
			}
			
			adj = null;
			adj = expanded;
		}
			
			adj[adjSize] = id;
			adjSize++;
		
	}
	
	public double lat(){
		return this.lat;
	}
	
	public int movementId(){
		return this.movementId;
	}
	
	public double lon(){
		return this.lon;
	}
	
	public void setVelocidadPromedio(double v){
		velocidadPromedio = v;
	}
	
	public double velocidadPromedio(){
		return velocidadPromedio;
	}
	
	/*
	public double calcularDistanciasArcos(){
		double distancias = 0;
		
		Iterable<Edge> arcos = verti.
		Iterator<Edge> iterador = arcos.iterator();
		while(iterador.hasNext()){
			Edge arcoActual = iterador.next();
			distancias += arcoActual.weight();
			iterador.next();
		}
		
		return distancias;
	}
	*/
	
	public String darKey(){
		return Integer.toString(id);
	}

	@Override
	public int compareTo(VerticeInfo o) {
		// TODO Auto-generated method stub
		
		if(this.velocidadPromedio<o.velocidadPromedio){
			return 1;
		}else if(this.velocidadPromedio>o.velocidadPromedio){
			return -1;
		}else{
			return 0;
		}
		
	}

	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public int getUniqueId(Long id, int size) {
		uniqueId = Math.abs(id.hashCode() % size);
		return uniqueId;
	}
	

}

