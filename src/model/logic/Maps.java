package model.logic;

import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.Polygon;
import com.teamdev.jxmaps.swing.MapView;
import java.awt.BorderLayout;
import java.util.Iterator;

import javax.swing.JFrame;

import model.data_structures.Queue;
import model.logic.VerticeInfo;
import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

/**
 * Esta clase representa los objetos y metodos basicos
 * necesarios para la realizacion de talleres y proyectos
 * que tengan que ver con dibujo de mapas
 * @author Juan Pablo Cano
 */
public class Maps extends MapView
{
	//-------------------------
	// Atributos
	//------------------------

	/**
	 * Mapa cargado de JxMaps
	 */
	private static final long serialVersionUID = 1L;
	private Map map;
	private boolean primera=true;
	private CircleOptions settingsCircle;
	private  PolylineOptions settingsLine;
	private VerticeInfo anterior =null;
	JFrame frame = new JFrame("Bogot� DC");

	/**
	 * Tabla de Hash que contiene las universidades en llaves
	 * y las Latitudes y Longitudes en Valor
	 */


	/**
	 * Arreglo de latitudes y longitudes
	 */

	//-----------------------
	// Constructores
	//---------------------

	/*Parte A // Parte B
	 *  4          7
	 */
	public Maps(Queue <Edge> que, VerticeInfo desde, VerticeInfo hasta,EdgeWeightedGraph<String, VerticeInfo>grapho) {


		setOnMapReadyHandler(new MapReadyHandler() {

			@Override
			public void onMapReady(MapStatus status) {
				// TODO Auto-generated method stub
				if(status == MapStatus.MAP_STATUS_OK) {
					settingsCircle=new CircleOptions();
					settingsCircle.setFillColor("#99d6ff");
					settingsCircle.setRadius(10);
					settingsCircle.setFillOpacity(0.35);
					settingsCircle.setStrokeColor("#0099ff");

					settingsLine=new PolylineOptions();
					settingsLine.setGeodesic(true);
					settingsLine.setStrokeColor("#0099ff");
					settingsLine.setStrokeOpacity(1.0);
					settingsLine.setStrokeWeight(2.0);

					map = getMap();
					MapOptions mapOptions = new MapOptions(); 
					MapTypeControlOptions controlOptions = new MapTypeControlOptions(); 
					controlOptions.setPosition(ControlPosition.RIGHT_TOP);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(4.6012, -74.0657));
					map.setZoom(11);

					generateArea(new LatLng(desde.lat(),desde.lon()), 10.0);
					generateArea(new LatLng(hasta.lat(),hasta.lon()), 10.0);
					for (Edge ed: que)
					{
						int idV = ed.either();
						int idW = ed.other2();
						LatLng in = new LatLng(grapho.getInfoVertex(Integer.toString(idV)).lat(),grapho.getInfoVertex(Integer.toString(idV)).lon());
						LatLng fin = new LatLng(grapho.getInfoVertex(Integer.toString(idW)).lat(),grapho.getInfoVertex(Integer.toString(idW)).lon());
						LatLng[] path = {in,fin};
						Polyline polyline = new Polyline(map);
						polyline.setPath(path);
						polyline.setOptions(settingsLine);
					}

				}

			}
		});

		frame.add(this, BorderLayout.CENTER);
		frame.setSize(900, 600);
		frame.setVisible(true);
	}
	/*Parte c
	 *  12
	 */
	public Maps(Queue <VerticeInfo> ruta, int inicio, int fin,EdgeWeightedGraph<String, VerticeInfo>grapho) {

		Iterator<VerticeInfo> it = ruta.iterator(); 
		primera = true; 
		anterior = null; 

		setOnMapReadyHandler(new MapReadyHandler() {

			@Override
			public void onMapReady(MapStatus status) {
				// TODO Auto-generated method stub
				int lati=0;
				int lon=0;
				if(status == MapStatus.MAP_STATUS_OK) {
					settingsCircle=new CircleOptions();
					settingsCircle.setFillColor("#ff9999");
					settingsCircle.setRadius(10);
					settingsCircle.setFillOpacity(0.35);
					settingsCircle.setStrokeColor("#ff9999");

					settingsLine=new PolylineOptions();
					settingsLine.setGeodesic(true);
					settingsLine.setStrokeColor("#000000");
					settingsLine.setStrokeOpacity(1.0);
					settingsLine.setStrokeWeight(2.0);

					map = getMap();
					MapOptions mapOptions = new MapOptions(); 
					MapTypeControlOptions controlOptions = new MapTypeControlOptions(); 
					controlOptions.setPosition(ControlPosition.RIGHT_TOP);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(4.6012, -74.0657));
					map.setZoom(11);

					while(it.hasNext()) {
						int id = (int) it.next().getId(); 
						VerticeInfo primero = grapho.getInfoVertex(""+id);

						if(primera) {

							String idSig = ""+it.next().getId(); 
							VerticeInfo siguiente = grapho.getInfoVertex(idSig); 
							lati=(int) primero.lat();
							lon = (int)primero.lon();
							LatLng in = new LatLng(primero.lat(), primero.lon());
							LatLng fin = new LatLng(siguiente.lat(), siguiente.lon());
							LatLng[] path = {in,fin};
							Polyline polyline = new Polyline(map);
							polyline.setPath(path);
							polyline.setOptions(settingsLine);
							anterior = siguiente; 
							primera = false; 
						}
						else {
							LatLng in = new LatLng(anterior.lat(), anterior.lon());
							LatLng fin = new LatLng(primero.lat(), primero.lon());
							LatLng[] path = {in,fin};
							Polyline polyline = new Polyline(map);
							polyline.setPath(path);
							polyline.setOptions(settingsLine);
							anterior = primero; 

						}

					}
				}

			}
		});

		frame.add(this, BorderLayout.CENTER);
		frame.setSize(900, 600);
		frame.setVisible(true);
	}
	/* Parte A
	 *   5
	 */
	public Maps(Queue <VerticeInfo> queue, EdgeWeightedGraph<String, VerticeInfo>grapho) {

System.out.println("entra");
		setOnMapReadyHandler(new MapReadyHandler() {

			@Override
			public void onMapReady(MapStatus status) {
				// TODO Auto-generated method stub
				if(status == MapStatus.MAP_STATUS_OK) {
					settingsCircle=new CircleOptions();
					settingsCircle.setFillColor("#ff6666");
					settingsCircle.setRadius(10);
					settingsCircle.setFillOpacity(0.35);
					settingsCircle.setStrokeColor("#e60000");

					settingsLine=new PolylineOptions();
					settingsLine.setGeodesic(true);
					settingsLine.setStrokeColor("#e60000");
					settingsLine.setStrokeOpacity(1.0);
					settingsLine.setStrokeWeight(2.0);

					map = getMap();
					MapOptions mapOptions = new MapOptions(); 
					MapTypeControlOptions controlOptions = new MapTypeControlOptions(); 
					controlOptions.setPosition(ControlPosition.RIGHT_TOP);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(4.6012, -74.0657));
					map.setZoom(11);
					int i=0;
					while(i < grapho.V())
					{
						VerticeInfo ve = grapho.getInfoVertex(Integer.toString(i));
						generateArea(new LatLng( ve.lat(),ve.lon()), 10.0);
						Iterator it = (Iterator<EdgeWeightedGraph<String, VerticeInfo>>) grapho.edges();
						while (it.hasNext())
						{
							Edge ed = (Edge) it.next();
							int idV = ed.either();
							int idW = ed.other2();
							LatLng in = new LatLng(grapho.getInfoVertex(Integer.toString(idV)).lat(),grapho.getInfoVertex(Integer.toString(idV)).lon());
							LatLng fin = new LatLng(grapho.getInfoVertex(Integer.toString(idW)).lat(),grapho.getInfoVertex(Integer.toString(idW)).lon());                                                                                                                                                                                     
							LatLng[] path = {in,fin};
							Polyline polyline = new Polyline(map);
							polyline.setPath(path);
							polyline.setOptions(settingsLine);
							System.out.println(in);
							System.out.println(fin);
						}
					}
					CircleOptions settingsCircle2=new CircleOptions();
					settingsCircle2.setFillColor("#73e600");
					settingsCircle2.setRadius(10);
					settingsCircle2.setFillOpacity(0.35);
					settingsCircle2.setStrokeColor("#66cc00");

					for (VerticeInfo ve: queue)
					{
						generateArea(new LatLng( ve.lat(),ve.lon()), 10.0, settingsCircle2);
					}
				}

			}
		});

		frame.add(this, BorderLayout.CENTER);
		frame.setSize(900, 600);
		frame.setVisible(true);
	}
   
	public void generateArea(LatLng center,Double radius)
	{
		Circle circle = new Circle(map);
		circle.setCenter(center);
		circle.setRadius(radius);
		circle.setVisible(true);
		circle.setOptions(settingsCircle);
	}
	/**
	 * Generate a circle area on the map
	 * @param center LatLong of the center of the map
	 * @param radius on meters
	 * @param circule options
	 */
	public void generateArea(LatLng center,Double radius,CircleOptions op)
	{
		Circle circle = new Circle(map);
		circle.setCenter(center);
		circle.setRadius(radius);
		circle.setVisible(true);
		circle.setOptions(op);
	}
	/**
	 * Inicializa el marco donde el mapa se va a cargar
	 * @param titulo El título del marco
	 */
	public void initFrame(String titulo)
	{
		JFrame frame = new JFrame(titulo);
		frame.setSize(700, 500);
		frame.setVisible(true);
		frame.add(this, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}