package model.logic;

public class Letra implements Comparable<Letra>{
	private char letra;
	private int reps;
	private String zonasLetraRepetida;

	public Letra(char letra, int reps, String zonasLetraRepetida){
		this.letra = letra;
		this.reps = reps;
		this.zonasLetraRepetida = zonasLetraRepetida;
	}
	
	public char getLetra(){
		return letra;
	}
	
	public int getReps(){
		return reps;
	}
	
	public String getZonasLetraRepetida(){
		return zonasLetraRepetida;
	}
	
	/**
	public int compareTo(Letra letra1, Letra letra2) {
		
		if(letra1.getReps() > letra2.getReps()){
			return 1;
		} else if(letra1.getReps() < letra2.getReps()){
			return -1;
		} else {
			return 0;
		}
	}
	*/

	public int compareTo(Letra letra2) {
		if(this.getReps() > letra2.getReps()){
			return 1;
		} else if(this.getReps() < letra2.getReps()){
			return -1;
		} else {
			return 0;
		}
	}
	
	
}
