package model.logic;

public class Adyacente {
	
	int id;
	double distancia;
	double tiempo;
	
	public  Adyacente(int id,double distancia, double tiempo){
		this.id = id;
		this.distancia = distancia;
		this.tiempo = tiempo;
	}
	
	public int id(){
		return this.id;
	}
	
	public double distancia(){
		return this.distancia;
	}
	
	public double tiempo(){
		return this.tiempo;
	}

}
