package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;

import javax.print.attribute.standard.Sides;

import model.algoritmos.BreadthFirstPaths;
import model.algoritmos.Dijkstra;
import model.algoritmos.Kruskal;
import model.data_structures.ArregloDinamico;
import model.data_structures.BinarySearchTree;
import model.data_structures.CC;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHash;

import com.google.gson.*;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	private EdgeWeightedGraph<String, VerticeInfo> verti;

	private ArregloDinamico<Viaje> viajesWeekly = new ArregloDinamico<Viaje>();

	private RedBlackBST<Integer, Nodo> nodos = new RedBlackBST<Integer, Nodo>();

	private RedBlackBST<Integer, Nodo> viajesSemanal = new RedBlackBST<Integer, Nodo>();

	SeparateChainingHash<String, Viaje> hash = new SeparateChainingHash<>();

	/*
	 * METODOS
	 */

	/**
	 * Carga el grafo
	 */
	public void cargar() {

		System.out.println("LOADING........");

		String line = "";

		// cargar malla vial
		System.out.println("Cargando malla vial");
		String line1 = "";
		int sizeMalla = 0;
		File fileMalla = new File("./data/Nodes_of_red_vial-wgs84_shp.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(fileMalla))) {
			br.readLine();
			line1 = br.readLine();
			while (line1 != null) {
				String[] data = line1.split(",");
				// id del nodo (key)
				int nodeId = Integer.parseInt(data[0]);

				// longitud
				double longitud = Double.parseDouble(data[1]);
				// latitud
				double latitud = Double.parseDouble(data[2]);

				sizeMalla++;
				Nodo nodoActual = new Nodo(nodeId, longitud, latitud);
				// nodosHash.put(nodeId, nodoActual);
				nodos.put(nodeId, nodoActual);
				line1 = br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Leyendo viajes por semana");
		File fileWeekly = new File("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv");
		try (BufferedReader br3 = new BufferedReader(new FileReader(fileWeekly))) {
			br3.readLine();
			line = br3.readLine();
			while (line != null) {
				String[] data = line.split(",");
				int dato1 = Integer.parseInt(data[0]);
				int dato2 = Integer.parseInt(data[1]);
				int dato3 = Integer.parseInt(data[2]);
				double dato4 = Double.parseDouble(data[3]);
				double dato5 = Double.parseDouble(data[4]);
				double dato6 = Double.parseDouble(data[5]);
				double dato7 = Double.parseDouble(data[6]);
				Viaje Viaje = new Viaje(dato1, dato2, dato3, dato4, dato5, dato6, dato7);
				hash.put(dato1 + dato2 + "", Viaje);
				// viajesWeekly.append(Viaje);
				// integer o string para las zonas??????

				// System.out.println("get nodos");
				// Nodo nodoOrigen = nodos.get(dato1);
				// Nodo nodoDestino = nodos.get(dato2);

				// System.out.println("get id de vertices");
				// int idOrigen =
				// encontrarIdVerticeCercano(nodoOrigen.getLatitud(),
				// nodoOrigen.getLongitud());
				// int idDestino =
				// encontrarIdVerticeCercano(nodoDestino.getLatitud(),
				// nodoDestino.getLongitud());

				// verti.setTime(idOrigen, idDestino, dato4);
				// VerticeInfo actual =
				// verti.getInfoVertex(Integer.toString(idOrigen));
				// VerticeInfo vecino =
				// verti.getInfoVertex(Integer.toString(idDestino));

				// System.out.println("set time");
				// verti.setTime(Integer.toString(idOrigen),
				// Integer.toString(idDestino), dato4);

				// verti.addEdge(e, e2, cost, tiempo);

				line = br3.readLine();
			}
			br3.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		verti = new EdgeWeightedGraph<String, VerticeInfo>(228045);
		try {

			BufferedReader br = new BufferedReader(new FileReader("data/bogota_vertices.txt"));
			String linea = br.readLine();
			linea = br.readLine();
			System.out.println("Creando vertices");
			while (linea != null) {
				String[] info = linea.split(";");
				// System.out.println(info[0]+";"+info[1]+";"+info[2]+";"+info[3]);
				double lat = Double.parseDouble(info[2]);
				double lon = Double.parseDouble(info[1]);
				// int movementId = encontrarIdMallaVial(lat, lon);
				VerticeInfo ver = new VerticeInfo(Integer.parseInt(info[0]), lat, lon, Integer.parseInt(info[3]), null);
				verti.setInfoVertex(ver.darKey(), ver);

				linea = br.readLine();
			}
			br.close();

			BufferedReader br2 = new BufferedReader(new FileReader("data/bogota_arcos.txt"));
			String linea2 = br2.readLine();
			int j = 0;
			System.out.println("Agregando arcos");
			while (linea2 != null) {

				String[] info = linea2.split(" ");
				VerticeInfo act = verti.getInfoVertex(info[0]);
				if (act != null) {

					if (info.length > 0) {
						for (int i = 1; i < info.length; i++) {
							VerticeInfo actual = verti.getInfoVertex(info[i]);
							if (actual != null) {
								double cost = Haversine.distance(act.lat(), act.lon(), actual.lat(), actual.lon());
								// System.out.println("movement ids");
								// System.out.println(act.movementId());
								// System.out.println(actual.movementId());
								// double time =
								// darTiempoDeArco(act.movementId(),
								// actual.movementId());
								double time = darTiempoDeArco(act.movementId(), actual.movementId());

								// System.out.println("Tiempo: " + time);
								// System.out.println(time);
								if (cost == 0) {
									System.out.println("Arco con peso 0");
								}

								act.addAdj(Integer.parseInt(info[i]));
								verti.addEdge(info[0], info[i], cost, time);

							}

						}
					}
				}
				linea2 = br2.readLine();
				j++;

			}

			br2.close();
		} catch (FileNotFoundException e) {
			System.out.println("Exception!");
			System.out.println(e);
		} catch (IOException e) {
			System.out.println("Exception!");
			System.out.println(e);
		}

		System.out.println("Agregando las velocidades a los vertices");

		int i = 1;
		while (i < verti.V()) {
			VerticeInfo actual = verti.getInfoVertex(Integer.toString(i));

			if (actual != null) {

				verti.setVelocidad(Integer.toString(i));
			}

			/*
			 * Iterator<Edge> iterador = verti.adj(i).iterator(); double
			 * velocidades = 0; int contador = 1; while (iterador.hasNext()) {
			 * Edge arcoActual = iterador.next(); velocidades +=
			 * arcoActual.velocidad(); //System.out.println(velocidades);
			 * contador++; }
			 * 
			 * double velocidadPromedio = (velocidades/contador); if(0 -
			 * velocidadPromedio == 0){ System.out.println(velocidadPromedio); }
			 * 
			 * actual.setVelocidadPromedio(velocidadPromedio); }
			 * 
			 */
			i++;
		}

		System.out.println("Cargue de Viajes por semana terminado");

		System.out.println("Total de vertices:");
		System.out.println(verti.V());
		System.out.println("Total de arcos:");
		System.out.println(verti.E());

		/*
		 * crearJson(); try { cargarJson(); } catch (Exception e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

	}

	public Queue<VerticeInfo> nodosDelimitados(double lonM, double lonMa, double latM, double latMa) {
		Queue<VerticeInfo> res = new Queue<VerticeInfo>();

		for (int i = 0; i < verti.V(); i++) {
			VerticeInfo actual = verti.getInfoVertex(Integer.toString(i));
			if (actual != null) {
				if (actual.lon() <= lonMa && actual.lon() >= lonM) {
					if (actual.lat() <= latM && actual.lat() >= latM) {
						res.enqueue(actual);
					}
				}
			}

		}

		return res;
	}

	public Queue<Edge> arcosDelimitados(Queue<VerticeInfo> cola, double lonM, double lonMa, double latM, double latMa) {
		Queue<VerticeInfo> copia = cola;
		Queue<Edge> res = new Queue<Edge>();
		for (int i = 0; i < cola.size(); i++) {
			int vertice = verti.hashT(copia.dequeue().darKey());
			Iterator<Edge> it = verti.adj(vertice).iterator();
			while (it.hasNext()) {
				Edge actual = it.next();
				VerticeInfo actualV = verti.getInfoVertex(Integer.toString(actual.other(vertice)));
				if (actualV != null) {
					if (actualV.lon() <= lonMa && actualV.lon() >= lonM) {
						if (actualV.lat() <= latM && actualV.lat() >= latM) {
							res.enqueue(actual);
						}
					}
				}
			}
		}
		return res;
	}

	private double darTiempoDeArco(int sourceId, int destId) {
		double tiempo = 0;

		// int sourceId = encontrarIdMallaVial(sourceLat, sourceLon);
		// int destId = encontrarIdMallaVial(destLat, destLon);

		if (sourceId == destId) {
			tiempo = 10;
		} else {
			Viaje viaje = hash.get(sourceId + destId + "");

			if (viaje != null) {
				tiempo = viaje.getMeanTravelTime();
			} else {
				tiempo = 100;
			}
		}
		/*
		 * Viaje viaje = hash.get(sourceId + destId + "");
		 * System.out.println(viaje); if(viaje != null){ tiempo =
		 * viaje.getMeanTravelTime(); }
		 */

		return tiempo;
	}

	@SuppressWarnings("unchecked")
	public void crearJson() {
		System.out.println("Crear Json");
		for (int i = 0; i <= verti.V(); i++) {
			VerticeInfo vertice = (VerticeInfo) verti.getInfoVertex(Integer.toString(i));
			JsonObject obj = new JsonObject();
			if (vertice != null) {
				obj.addProperty("id", vertice.id());
				obj.addProperty("latitud", vertice.lat());
				obj.addProperty("longitud", vertice.lon());
				obj.addProperty("movementId", vertice.movementId());
			} else {
				obj.addProperty("id", "");
				obj.addProperty("latitud", 0);
				obj.addProperty("longitud", 0);
				obj.addProperty("movementId", 0);
			}

			JsonArray list = new JsonArray();
			Iterable<Edge> iterable = verti.adj(i);
			if (iterable != null) {
				Iterator<Edge> itr = iterable.iterator();
				while (itr.hasNext()) {
					JsonObject obj2 = new JsonObject();
					Edge arco = itr.next();
					int adj = arco.other(i);
					double distancia = arco.weight();
					double tiempo = arco.tiempo();
					double velocidad = arco.velocidad();

					obj2.addProperty("id", adj);
					obj2.addProperty("distancia", distancia);
					obj2.addProperty("tiempo", tiempo);
					obj2.addProperty("velocidad", velocidad);
					list.add(obj2);
				}
			}
			// cuidado con esta linea! si falla el metodo, es aqui
			obj.addProperty("adyacentes", list.getAsString());

			try {
				FileWriter file = new FileWriter("data/nodos.json");
				file.write(obj.toString());
				file.flush();

				file.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void cargarJson() throws Exception {
		System.out.println("Cargar Json");
		verti = new EdgeWeightedGraph<String, VerticeInfo>(228046);
		ChargeManager actual[] = null;
		try {
			BufferedReader reader = new BufferedReader(new FileReader("data/nodos.json"));
			actual = new Gson().fromJson(reader, ChargeManager[].class);
		} catch (Exception e) {
			System.out.println(e.getMessage() + "error");
		}

		for (int i = 0; i < actual.length; i++) {

			int id = actual[i].id();
			double latitud = actual[i].lat();
			double longitud = actual[i].lon();
			int movementId = actual[i].movementId();
			Adyacente[] adyacentes = actual[i].adj();
			// cargar

		}
	}

	public int componentesConectados() {
		return verti.cC();
	}

	public int encontrarIdVerticeCercano(double latitud, double longitud) {

		double lowestCost = 1000000000;

		int id = 0;

		int i = 0;

		while (i < verti.V()) {
			VerticeInfo actual = verti.getInfoVertex(Integer.toString(i));
			if (actual != null) {
				double cost = Haversine.distance(latitud, longitud, actual.lat(), actual.lon());
				if (cost < lowestCost) {
					lowestCost = cost;
					id = actual.id();
				}
			}

			i++;
		}

		System.out.println("Id");
		System.out.println(id);

		return id;

	}

	private int encontrarIdMallaVial(double latitud, double longitud) {

		double lowestCost = 1000000000;

		int id = 0;

		int i = 0;

		while (i < nodos.size()) {
			Nodo actual = nodos.get(i);
			if (actual != null) {
				double cost = Haversine.distance(latitud, longitud, actual.getLatitud(), actual.getLongitud());
				if (cost < lowestCost) {
					lowestCost = cost;
					id = actual.getId();
				}
			}

			i++;
		}

		return id;

	}
	//4a
	public void encontrarCaminoCostoMinimo(double latitudOrigen, double longitudOrigen, double latitudDestino,
			double longitudDestino) {

		int idZonaOrigen = encontrarIdVerticeCercano(latitudOrigen, longitudOrigen);
		VerticeInfo verticeOrigen = verti.getInfoVertex(Integer.toString(idZonaOrigen));
		double latitudVerticeOrigen = verticeOrigen.lat;
		double longitudVerticeOrigen = verticeOrigen.lon();

		int idZonaDestino = encontrarIdVerticeCercano(latitudDestino, longitudDestino);
		VerticeInfo verticeDestino = verti.getInfoVertex(Integer.toString(idZonaDestino));
		double latitudVerticeDestino = verticeDestino.lat;
		double longitudVerticeDestino = verticeDestino.lon();
		Queue<Edge> que = caminoCosto(idZonaOrigen,idZonaDestino);
		Maps mapa = new Maps(que, verticeOrigen, verticeDestino, verti);
	}

	private Queue<Edge> caminoCosto(int idZonaOrigen, int idZonaDestino) {
		// TODO Auto-generated method stub
		Dijkstra yo = new Dijkstra();
		return (Queue<Edge>) yo.minimoCaminoTiempo(verti,verti.getInfoVertex(""+idZonaOrigen), verti.getInfoVertex(""+idZonaDestino));
	}

	
	
	
	//7b
	public void encontrarCaminoCostoMininmoHaversine(double latitudOrigen, double longitudOrigen, double latitudDestino,
			double longitudDestino) {

		int idZonaOrigen = encontrarIdVerticeCercano(latitudOrigen, longitudOrigen);
		VerticeInfo verticeOrigen = verti.getInfoVertex(Integer.toString(idZonaOrigen));
		double latitudVerticeOrigen = verticeOrigen.lat;
		double longitudVerticeOrigen = verticeOrigen.lon();

		int idZonaDestino = encontrarIdVerticeCercano(latitudDestino, longitudDestino);
		VerticeInfo verticeDestino = verti.getInfoVertex(Integer.toString(idZonaDestino));
		double latitudVerticeDestino = verticeDestino.lat;
		double longitudVerticeDestino = verticeDestino.lon();
		Queue<Edge> que = caminoCostohaversine(idZonaOrigen,idZonaDestino);
		Maps mapa = new Maps(que, verticeOrigen, verticeDestino, verti);
	}

	private Queue<Edge> caminoCostohaversine(int idZonaOrigen, int idZonaDestino) {
		// TODO Auto-generated method stub
		Dijkstra yo = new Dijkstra();
		return (Queue<Edge>) yo.minimoCaminoHaversine(verti,verti.getInfoVertex(""+idZonaOrigen), verti.getInfoVertex(""+idZonaDestino));
	}
	public void determinarNVerticesConMenorVelocidad(int n) {

		int v = 1;

		MaxPQ<VerticeInfo> verticesPQ = new MaxPQ<>();
		Queue<VerticeInfo> hola = new Queue<>();
		while (v < verti.V()) {

			VerticeInfo verticeActual = verti.getInfoVertex(Integer.toString(v));
			if (verticeActual != null) {
				verticesPQ.insert(verticeActual);
				hola.enqueue(verticeActual);
			}

			v++;
		}

		Iterator<VerticeInfo> iterador = verticesPQ.iterator();
		int contador = 0;
		while (iterador.hasNext() && contador < n) {

			VerticeInfo actual = iterador.next();
			System.out.println("Vertice #" + (contador + 1));

			System.out.println("id: " + actual.id());
			System.out.println("Lat: " + actual.lat());
			System.out.println("Lon: " + actual.lon());
			System.out.println("Velocidad: " + actual.velocidadPromedio() + "\n");

			contador++;
		}
		
		Maps mapa = new Maps(hola, verti);
	}

	public void calcularMSTPrim() {

	}

	public void encontrarCaminoMenorCosto(double latitudOrigen, double longitudOrigen, double latitudDestino,
			double longitudDestino) {

		int idOrigen = encontrarIdVerticeCercano(latitudOrigen, longitudOrigen);
		int idDestino = encontrarIdVerticeCercano(latitudDestino, longitudDestino);

		VerticeInfo verticeOrigen = (VerticeInfo) verti.getInfoVertex(Integer.toString(idOrigen));
		VerticeInfo verticeDestino = (VerticeInfo) verti.getInfoVertex(Integer.toString(idDestino));

		BreadthFirstPaths bfs = new BreadthFirstPaths(verti, verticeOrigen);
		Queue<VerticeInfo> queue = new Queue<>();
		// QUITAR BFS E IMPLEMENTAR CON HAVERSINE!!!!!
		Iterable<VerticeInfo> resultado = bfs.pathTo(verticeDestino);

		if (resultado != null) {
			Iterator<VerticeInfo> iterator = resultado.iterator();

			while (iterator.hasNext()) {
				VerticeInfo actual = iterator.next();
				if (actual != null) {
					queue.enqueue(actual);
					System.out.println(actual.getId());
				}
			}
		} else {
			System.out.println("No hay camino entre los 2 vertices");
		}
		Maps mapa = new Maps(queue, verti);
	}

	public void encontrarVerticesAlcanzables(int t) {
		
	}

	public void calcularMSTKruskal() {
		
		Kruskal k = new Kruskal();
		
		//CC cc = new CC(verti);
		
		Queue<Edge> resultado = k.getMst(verti);
		
		Iterator<Edge> it = resultado.iterator();
		
		double pesoTotal = 0;
		
		while(it.hasNext()){
			Edge actual = it.next();
			pesoTotal += actual.weight();
			System.out.println("Vertices:");
			System.out.println(actual.either());
			System.out.println(actual.other(actual.either()));
			System.out.println("\n");
		}
		
		System.out.println("Distancia total: " + pesoTotal);
	}

	public void construirGrafoSimplificado() {

		System.out.println("Tiempo que toma el algoritmo en encontrar la solución:");
		System.out.println("Secuencia de vértices/zonas del camino resultante:");
		System.out.println("Tiempo promedio de zona de origen a zona destino:");
	}

	public void calcularCaminoCostoMinimo(int zonaOrigen, int zonaDestino) {

	}

	public void calcularCaminosMenorLongitud(int idOrigen) {

		VerticeInfo verticeOrigen = (VerticeInfo) verti.getInfoVertex(Integer.toString(idOrigen));

		int idDestino = 1;
		int contador = 1;
		//Queue<VerticeInfo> path = new Queue<>();
		BreadthFirstPaths bfs = new BreadthFirstPaths(verti, verticeOrigen);
		
		Iterator<VerticeInfo> resultadoMasLargo = null;
		int cantidadResultadoMasLargo = 0;
		
		while (idDestino < verti.V()) {

			VerticeInfo verticeDestino = (VerticeInfo) verti.getInfoVertex(Integer.toString(idDestino));

			// path=(Queue<VerticeInfo>) bfs.pathTo(verticeDestino);
			Iterable<VerticeInfo> resultado = bfs.pathTo(verticeDestino);

			if (resultado != null) {
				System.out.println("Resultado #" + contador);
				System.out.println("Camino:");
				Iterator<VerticeInfo> iterator = resultado.iterator();
				int cantidadActual = 0;

				while (iterator.hasNext()) {
					VerticeInfo actual = iterator.next();
					if (actual != null) {
						cantidadActual++;
						System.out.println(actual.getId());
					}
				}

				if (cantidadActual > cantidadResultadoMasLargo) {
					cantidadResultadoMasLargo = cantidadActual;
					resultadoMasLargo = iterator;
				}

				contador++;
			}
			idDestino++;
		}
		
		//anadir el mas largo a la cola del mapa
		Queue<VerticeInfo> path = new Queue<>();
		while(resultadoMasLargo.hasNext()){
			VerticeInfo actual = resultadoMasLargo.next();
			path.enqueue(actual);
		}
		
		Maps mapa3 = new Maps(path, verticeOrigen.getId(), verticeOrigen.getId(), verti);
	}

	public EdgeWeightedGraph<String, VerticeInfo> getGrapho() {
		// TODO Auto-generated method stub
		return verti;
	}

}