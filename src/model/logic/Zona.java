package model.logic;

import java.util.Comparator;

public class Zona {

	//---------
	//Atributos
	//---------
	
	/**
	 * ID de la zona
	 */
	private int movementId;
	
	/**
	 * nombre de la zona
	 */
	private String scaNombre;
	
	/**
	 *  perímetro de la zona, área de la zona
	 */
	private double shapeLeng, shapeArea;
	
	/**
	 *  coordenadas
	 */
	private Double[][] coordenadas;
	
	public Zona(int p1, String p2,double p3,double p4, Double[][] p5){
		movementId=p1;
		scaNombre=p2;
		shapeLeng=p3;
		shapeArea=p4;
		coordenadas=p5;
	}
	
	public int getMovementId()
	{
		return movementId;
	}
	
	public String getScaNombre()
	{
		return scaNombre;
	}
	
	public double getShapeLeng()
	{
		return shapeLeng;
	}
	
	public double getShapeArea()
	{
		return shapeArea;
	}
	
	public Double[][] getCoordenadas()
	{
		return coordenadas;
	}


	public int compareTo(Zona other) {
		
		return 1;
	}

	

}
