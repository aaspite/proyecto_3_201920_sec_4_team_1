package model.logic;

import java.util.Comparator;

public class ComparadorZonasLetras implements Comparator<Zona>{
		
		public ComparadorZonasLetras(){
			
		}

		@Override
		public int compare(Zona zona1, Zona zona2) {
			
			String nombreZona1 = zona1.getScaNombre();
			String nombreZona2 = zona2.getScaNombre();
			
			return nombreZona1.compareToIgnoreCase(nombreZona2);
			
		}
}

