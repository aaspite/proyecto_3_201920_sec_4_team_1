package model.algoritmos;

import java.util.Iterator;

import model.data_structures.IndexMinPQ;
import model.data_structures.MaxPQ;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHash;

import model.data_structures.Queue;
import model.logic.Edge;
import model.logic.EdgeWeightedGraph;
import model.logic.VerticeInfo;

/**
 *  The {@code DijkstraUndirectedSP} class represents a data type for solving
 *  the single-source shortest paths problem in edge-weighted graphs
 *  where the edge weights are nonnegative.
 *  <p>
 *  This implementation uses Dijkstra's algorithm with a binary heap.
 *  The constructor takes &Theta;(<em>E</em> log <em>V</em>) time in the
 *  worst case, where <em>V</em> is the number of vertices and
 *  <em>E</em> is the number of edges.
 *  Each instance method takes &Theta;(1) time.
 *  It uses &Theta;(<em>V</em>) extra space (not including the
 *  edge-weighted graph).
 *  <p>
 *  For additional documentation,    
 *  see <a href="https://algs4.cs.princeton.edu/44sp">Section 4.4</a> of    
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne. 
 *  See {@link DijkstraSP} for a version on edge-weighted digraphs.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 *  @author Nate Liu
 */
public class Dijkstra {
	private Double distanciaTo []; 
	private Edge arcosTo [];
	private SeparateChainingHash<Integer,Double> distTo;          // distTo[v] = distance  of shortest s->v path
	private SeparateChainingHash<Integer,Edge> edgeTo;            // edgeTo[v] = last edge on shortest s->v path
	private IndexMinPQ<Double> pq;    // priority queue of vertices
	private RedBlackBST<Integer, VerticeInfo> pqAux; 
	private SeparateChainingHash<Long,Integer> longToInteger;
	private Long[] IntegerToLong;
	/**
	 * Computes a shortest-paths tree from the source vertex {@code s} to every
	 * other vertex in the edge-weighted graph {@code G}.
	 *
	 * @param  G the edge-weighted digraph
	 * @param  s the source vertex
	 * @throws IllegalArgumentException if an edge weight is negative
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public Dijkstra(EdgeWeightedGraph<String,VerticeInfo> G, VerticeInfo s) {

		distTo = new SeparateChainingHash<>();
		edgeTo = new SeparateChainingHash<>();



		for (int i = 0; i < G.V(); i++)
		{
			VerticeInfo v = G.getInfoVertex(Integer.toString(i));
			distTo.put(s.getUniqueId((long) v.getId(), G.V()), Double.POSITIVE_INFINITY);
		}
		distTo.put(s.getUniqueId((long) s.getId(), G.V()), 0.0);

		// relax vertices in order of distance from s
		pq = new IndexMinPQ(G.V());
		pqAux = new RedBlackBST<>();
		pq.insert(s.getUniqueId((long) s.getId(), G.V()), distTo.get(s.getUniqueId((long) s.getId(), G.V())));
		pqAux.put(s.getUniqueId((long) s.getId(), G.V()), s);

		while(!pq.isEmpty()) {

			VerticeInfo v =  (VerticeInfo) G.getVertices().get(pq.delMin());
			pq.delMin();

			Iterator<Edge> iter = (Iterator<Edge>) G.adj(v.getId()); 
			while(iter.hasNext()) {
				Edge e = iter.next();
				relax(e, G.V(),G); 
			}
			pqAux.deleteMin();
		}
	}



	/**
	 * Returns the length of a shortest path between the source vertex {@code s} and
	 * vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return the length of a shortest path between the source vertex {@code s} and
	 *         the vertex {@code v}; {@code Double.POSITIVE_INFINITY} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	

	public Dijkstra() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * Returns true if there is a path from the source vertex {@code s} to vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @param size total de vertices en el grafo
	 * @return {@code true} if there is a path from the source vertex
	 *         {@code s} to vertex {@code v}; {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(VerticeInfo v, int size) {
		return distTo.get(v.getUniqueId((long) v.getId(), size)) < Double.POSITIVE_INFINITY; 
	}

	/**
	 * Returns a shortest path between the source vertex {@code s} and vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return a shortest path between the source vertex {@code s} and vertex {@code v};
	 *         {@code null} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Iterable<Edge> pathTo(VerticeInfo v,int size,EdgeWeightedGraph<String,VerticeInfo> G) {

		if (!hasPathTo(v,size)) return null;
		int key = v.getUniqueId((long) v.getId(), size);
		Queue<Edge> path = new Queue<>(); 
		for(Edge e = edgeTo.get(key); e != null; e = edgeTo.get(G.getInfoVertex(""+e.either()).getUniqueId((long) G.getInfoVertex(""+e.either()).getId(), size))) {
			path.enqueue(e);
		}

		return path;
	}

	public Iterable <Edge> minimoCaminoHaversine (EdgeWeightedGraph<String,VerticeInfo> G, VerticeInfo s, VerticeInfo ll) {
		distanciaTo = new Double[G.E()];
		arcosTo = new Edge [G.E()] ; 
		longToInteger = new SeparateChainingHash<>();
		IntegerToLong = new Long [G.E()];
		Queue <Edge> res = new Queue <Edge> ();
		int j=0;
		while(j<G.V())
		{
			VerticeInfo v = G.getInfoVertex(""+j);
			int i=longToInteger.size();
			IntegerToLong[i]=(long) v.getId();
			longToInteger.put((long) v.getId(),i);
			distanciaTo[i]= Double.POSITIVE_INFINITY;
		}
		int peque = longToInteger.get((long) s.getId());
		distanciaTo[peque] =(double) s.hashCode();
		pq = new IndexMinPQ(G.V());
		pq.insert(peque, distanciaTo[peque]);


		while(!pq.isEmpty()) {
			double valor = pq. minKey();
			Integer este= pq.delMin();
			VerticeInfo v =  G.getInfoVertex(""+IntegerToLong[este]);
			Iterator<Edge> iter = (Iterator<Edge>) G.adj(v.getId()); 
			while(iter.hasNext()) {
				Edge e = iter.next();
				relax(e, G.V(),valor+e.weight(), v); }
		}
		Edge estoy = arcosTo[longToInteger.get((long)ll.getId())];
		while (estoy!= null &&ll!=s){
			ll= G.getInfoVertex(""+estoy.other(ll.getId()));
			res.enqueue(estoy);
			estoy = arcosTo[longToInteger.get((long)ll.getId())];
			}
		return res;
	}
	
	public Iterable <Edge> minimoCaminoTiempo (EdgeWeightedGraph<String,VerticeInfo> G, VerticeInfo s, VerticeInfo ll) {
		distanciaTo = new Double[G.E()];
		arcosTo = new Edge [G.E()] ; 
		longToInteger = new SeparateChainingHash<>();
		IntegerToLong = new Long [G.E()];
		Queue <Edge> res = new Queue <Edge> ();
		int j=0;
		while(j<G.V())
		{
			VerticeInfo v = G.getInfoVertex(""+j);
			int i=longToInteger.size();
			IntegerToLong[i]=(long) v.getId();
			longToInteger.put((long) v.getId(),i);
			distanciaTo[i]= Double.POSITIVE_INFINITY;
		}
		int peque = longToInteger.get((long) s.getId());
		distanciaTo[peque] =(double) s.hashCode();
		pq = new IndexMinPQ(G.V());
		pq.insert(peque, distanciaTo[peque]);


		while(!pq.isEmpty()) {
			double valor = pq. minKey();
			Integer este= pq.delMin();
			VerticeInfo v =  G.getInfoVertex(""+IntegerToLong[este]);
			Iterator<Edge> iter = (Iterator<Edge>) G.adj(v.getId()); 
			while(iter.hasNext()) {
				Edge e = iter.next();
				relax(e, G.V(),valor+e.tiempo(), v); }
		}
		Edge estoy = arcosTo[longToInteger.get((long)ll.getId())];
		while (estoy!= null &&ll!=s){
			ll= G.getInfoVertex(""+estoy.other(ll.getId()));
			res.enqueue(estoy);
			estoy = arcosTo[longToInteger.get((long)ll.getId())];
			}
		return res;
	}
	
	
	private void relax(Edge e, int size, double valor, VerticeInfo v) {
		int  w = e.other(v.getId()); 

		int minW = longToInteger.get((long) w);
		if(distanciaTo[minW] > valor) {
			distanciaTo[minW]= valor;
			arcosTo[minW]= e;
			if(pq.contains(minW)) {
				pq.decreaseKey(minW, distanciaTo[minW]);
			}
			else {
				pq.insert(minW, distanciaTo[minW]);
			}

		}
	}
	private void relax(Edge e, int size,EdgeWeightedGraph<String, VerticeInfo>G) {
		VerticeInfo v = G.getInfoVertex(""+e.either()), w = G.getInfoVertex(""+e.other(v.getId())); 
		int minW =w.getUniqueId((long) w.getId(),size);
		int minV = v.getUniqueId((long) v.getId(), size);
		if(distTo.get(minW) > distTo.get(minV) +(Double) e.weight()) {
			distTo.put(minW, distTo.get(minV)+(Double)e.weight());
			edgeTo.put(minW, e);
			if(pq.contains(minW)) {
				pq.decreaseKey(minW, distTo.get(minW));
			}
			else {
				pq.insert(minW, distTo.get(minW));

			}
		}
	}

	/**
	 * Returns the length of a shortest path from the source vertex {@code s} to vertex {@code v}.
	 * @param  v the destination vertex
	 * @param size total de vertices en el grafo
	 * @return the length of a shortest path from the source vertex {@code s} to vertex {@code v};
	 *         {@code Double.POSITIVE_INFINITY} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public double distTo(VerticeInfo v, int size) {
		return distTo.get(v.getUniqueId((long) v.getId(), size));
	}

	

}