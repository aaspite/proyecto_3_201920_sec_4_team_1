package model.algoritmos;

import java.util.Iterator;

import model.estructuras.Graph;
import model.estructuras.SeparateChainingHash;
import model.estructuras.Graph.Vertex;

public class DepthFirstPaths {

	private SeparateChainingHash<Long, Boolean> marked; 
	private SeparateChainingHash<Long, Vertex> edgeTo; 
	private final Vertex s; 


	public DepthFirstPaths(Graph G, Vertex s) {
		this.s = s; 
		edgeTo = new SeparateChainingHash<>(); 
		marked = new SeparateChainingHash<>();
		dfs(G,s); 
	}

	private void dfs(Graph G, Vertex v) {
		marked.put((Long) v.getId(), true);
		Iterator<Vertex> it = G.adj(v.getId());
		while(it.hasNext()) {
			Vertex w = it.next(); 
			if(!marked.get((Long) w.getId())) {
				edgeTo.put((Long) w.getId(), v);
				dfs(G,w);
			}
		}
	}

	/**
	 * Is there a path between the source vertex {@code s} and vertex {@code v}?
	 * @param v the vertex
	 * @return {@code true} if there is a path, {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(Vertex v) {
		return marked.get((Long) v.getId());
	}


}
