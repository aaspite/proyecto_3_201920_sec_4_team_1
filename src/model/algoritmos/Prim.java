package model.algoritmos;

import java.util.Iterator;

import model.estructuras.Graph;
import model.estructuras.Graph.Edge;
import model.estructuras.Graph.Vertex;
import model.estructuras.IndexMinPQ;
import model.estructuras.Queue;
import model.estructuras.RedBlackBST;
import model.estructuras.SeparateChainingHash;

public class Prim {
	private static final double FLOATING_POINT_EPSILON = 1E-12;

	private SeparateChainingHash<Long, Edge> EdgeTo; 
	private SeparateChainingHash<Integer, Double> distTo; 
	private SeparateChainingHash<Integer, Boolean> marked; 
	private IndexMinPQ<Double> pq; 
	private Double[] distanciaTo;
	private SeparateChainingHash<Long,Integer > longToInteger;
	private Long IntegerToLong [];
	private RedBlackBST<Long, Vertex> datos; 

	public Prim() {

	}

	public void getMST(Graph G) {
		EdgeTo = new SeparateChainingHash<>(); 
		distTo = new SeparateChainingHash<>(); 
		marked = new SeparateChainingHash<>(); 
		datos = new RedBlackBST<>(); 
		longToInteger = new SeparateChainingHash<>();
		IntegerToLong = new Long[G.E()];
		distanciaTo = new Double[G.E()];
		pq = new IndexMinPQ<>(G.V()); 

		Iterator<Long> it = G.getMyVertices().keys().iterator(); 
		while(it.hasNext()) {
			Vertex v = (Vertex) G.getMyVertices().get(it.next());
			int i = longToInteger.size();
			IntegerToLong[i] = (Long) v.getId();
			longToInteger.put((Long) v.getId(), i);
			if(!longToInteger.contains((Long) v.getId())) {
				IntegerToLong[longToInteger.size()] = (Long)v.getId();
				longToInteger.put((Long)v.getId(), longToInteger.size());
			}
			distanciaTo[i] = Double.POSITIVE_INFINITY;
		}

		Iterator<Long> iter = G.getMyVertices().keys().iterator(); 
		while(iter.hasNext()) {
			Vertex v = G.getVertex(iter.next()); 
			marked.put(longToInteger.get((Long) v.getId()), false);
			if(!marked.get(longToInteger.get((Long)v.getId()))) {
				prim(G,v); 
			}
		}
	}

	private void prim(Graph G, Vertex s) {
		int sou = longToInteger.get((Long) s.getId());
		distanciaTo[sou] = 0.0;
		pq.insert(sou, distanciaTo[sou]);

		while(!pq.isEmpty()) {

			Integer este = pq.delMin();

			Vertex v = (Vertex) G.getMyVertices().get(IntegerToLong[este]);
			scan(G,v); 
		}
	}

	private void scan(Graph G, Vertex v) {
		marked.put(longToInteger.get((Long) v.getId()), true);
		Iterator<Edge> it = G.edges(v.getId());
		while(it.hasNext()) {
			Edge e = it.next();
			Vertex w = e.other(v); 
			int minW = longToInteger.get((Long) w.getId());
			if(!marked.get(longToInteger.get((Long) v.getId()))) {
				continue;
			}
			if((Double) e.getInfo() <distanciaTo[minW]) {
				distanciaTo[minW] = (Double) e.getInfo();
				EdgeTo.put((Long) w.getId(), e);
				if(pq.contains(minW)) {
					pq.decreaseKey(minW, distanciaTo[minW]);
				}
				else {
					pq.insert(minW, distanciaTo[minW]);
				}
			}
		}
	}

	public Iterable <Edge> edges(Graph G){
		Queue<Edge> mst = new Queue<>(); 
		Iterator<Long> it = G.getMyVertices().keys().iterator();

		while(it.hasNext()) {
			Vertex v = G.getVertex(it.next());
			Edge e = EdgeTo.get((Long) v.getId());
			if(e != null) {
				mst.enqueue(e);
			}
		}

		return mst;
	}


}
