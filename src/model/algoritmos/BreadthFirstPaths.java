package model.algoritmos;

import java.util.Iterator;

import model.data_structures.Queue;
import model.data_structures.SeparateChainingHash;
import model.logic.Edge;
import model.logic.EdgeWeightedGraph;
import model.logic.VerticeInfo;

public class BreadthFirstPaths {
	private static final int INFINITY = Integer.MAX_VALUE;
	private SeparateChainingHash<Integer, Boolean> marked;
	private SeparateChainingHash<Integer, VerticeInfo> edgeTo;
	private SeparateChainingHash<Integer, Integer> disTo;
	private Queue<EdgeWeightedGraph<String, VerticeInfo>> subGraphs;

	/**
	 * Computes the shortest path between the source VerticeInfo {@code s} and
	 * every other VerticeInfo in the graph {@code G}.
	 * 
	 * @param G
	 *            the graph
	 * @param s
	 *            the source VerticeInfo
	 * @throws IllegalArgumentException
	 *             unless {@code 0 <= s < V}
	 */
	public BreadthFirstPaths(EdgeWeightedGraph<String, VerticeInfo> G, VerticeInfo s) {
		marked = new SeparateChainingHash<>();
		disTo = new SeparateChainingHash<>();
		edgeTo = new SeparateChainingHash<>();

		bfs(G, s);
	}

	/**
	 * Inicializar
	 */
	public BreadthFirstPaths() {
		marked = new SeparateChainingHash<>();
		disTo = new SeparateChainingHash<>();
		edgeTo = new SeparateChainingHash<>();
	}

	public Queue<EdgeWeightedGraph<String, VerticeInfo>> subgrafos(EdgeWeightedGraph<String, VerticeInfo> g,
			Iterable<VerticeInfo> vertices) {
		subGraphs = new Queue<EdgeWeightedGraph<String, VerticeInfo>>();
		for (VerticeInfo v : vertices) {
			marked.put((int) v.getId(), false);
		}
		for (VerticeInfo v : vertices) {
			EdgeWeightedGraph<String, VerticeInfo> estesub = new EdgeWeightedGraph<String, VerticeInfo>(230000);
			if (!marked.get(v.getId())) {
				Queue<VerticeInfo> que = new Queue<VerticeInfo>();
				que.enqueue(v);
				estesub.setInfoVertex(Integer.toString(v.getId()), v);

				while (!que.isEmpty()) {
					VerticeInfo estoy = que.dequeue();
					if (marked.get((int) estoy.getId()))
						continue;

					marked.put((int) estoy.getId(), true);
					Iterable<Edge> edgs = g.edges();
					Iterator<Edge> it = edgs.iterator();

					while (it.hasNext()) {

						Edge edge = it.next();
						int j = edge.other(estoy.getId());
						if (marked.contains(j)) {

							if (!marked.get(j)) {
								que.enqueue((VerticeInfo) g.getInfoVertex(Integer.toString(j)));
								estesub.setInfoVertex(Integer.toString(j),
										(VerticeInfo) g.getInfoVertex(Integer.toString(edge.other(estoy.getId()))));
								estesub.addEdge(Integer.toString(estoy.getId()), Integer.toString(j), edge.weight(),
										edge.tiempo());
							} else {
								estesub.addEdge(Integer.toString(estoy.getId()), Integer.toString(j), edge.weight(),
										edge.tiempo());
							}

						}

					}
				}
				subGraphs.enqueue(estesub);
			}
		}
		return subGraphs;

	}

	/**
	 * Computes the shortest path between any one of the source vertices in
	 * {@code sources} and every other VerticeInfo in graph {@code G}.
	 * 
	 * @param G
	 *            the graph
	 * @param sources
	 *            the source vertices
	 * @throws IllegalArgumentException
	 *             unless {@code 0 <= s < V} for each VerticeInfo {@code s} in
	 *             {@code sources}
	 */
	public BreadthFirstPaths(EdgeWeightedGraph<String, VerticeInfo> G, Iterable<VerticeInfo> sources) {
		marked = new SeparateChainingHash<>();
		disTo = new SeparateChainingHash<>();
		edgeTo = new SeparateChainingHash<>();

		int i = 0;

		while (i < G.V()) {
			VerticeInfo actual = (VerticeInfo) G.getInfoVertex(Integer.toString(i));
			if (actual != null) {
				disTo.put(actual.getId(), INFINITY);
			}

			i++;
		}

		bfs(G, sources);
	}

	private void bfs(EdgeWeightedGraph<String, VerticeInfo> G, VerticeInfo s) {
		Queue<VerticeInfo> q =  new Queue<>(); 
		
		int i = 0;
		
		while(i < G.V()){
			VerticeInfo actual = (VerticeInfo) G.getInfoVertex(Integer.toString(i));
			if(actual != null){
				disTo.put( i, INFINITY);
				marked.put( i, false);
			}
			
			i++;
		}
		
		disTo.put(s.getId(),0);
		marked.put( s.getId(), true);
		q.enqueue(s);

		while(!q.isEmpty()) {
			VerticeInfo v = q.dequeue(); 
			
			int[] adj = v.adj();
			for (int j = 0; j < adj.length; j++) {
				VerticeInfo w = (VerticeInfo) G.getInfoVertex(Integer.toString(adj[j]));
				if(!marked.get((int) w.getId())) {
					edgeTo.put( w.getId(), v);
					//+1 ???
					disTo.put( w.getId(), disTo.get(v.getId())+1);
					marked.put( w.getId(), true);
					q.enqueue(w);
				}
			}
		}
	}

	private void bfs(EdgeWeightedGraph<String, VerticeInfo> G, Iterable<VerticeInfo> sources) {
		Queue<VerticeInfo> q = new Queue<>();
		Iterator<VerticeInfo> it = sources.iterator();
		while (it.hasNext()) {
			VerticeInfo s = it.next();
			marked.put((int) s.getId(), true);
			disTo.put((int) s.getId(), 0);
			q.enqueue(s);
		}

		while (!q.isEmpty()) {
			VerticeInfo v = q.dequeue();
			
			int[] adj = v.adj();
			for (int j = 0; j < adj.length; j++) {
				VerticeInfo w = (VerticeInfo) G.getInfoVertex(Integer.toString(adj[j]));
				if (!marked.get( w.getId())) {
					edgeTo.put( w.getId(), v);
					disTo.put( w.getId(), disTo.get((int) v.getId()) + 1);
					marked.put( w.getId(), true);
					q.enqueue(w);
				}
			}
		}

	}

	/**
	 * Is there a path between the source VerticeInfo {@code s} (or sources) and
	 * VerticeInfo {@code v}?
	 * 
	 * @param v
	 *            the VerticeInfo
	 * @return {@code true} if there is a path, and {@code false} otherwise
	 * @throws IllegalArgumentException
	 *             unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(VerticeInfo v) {
		return marked.get(v.getId());
	}

	/**
	 * Returns a shortest path between the source VerticeInfo {@code s} (or
	 * sources) and {@code v}, or {@code null} if no such path.
	 * 
	 * @param v
	 *            the VerticeInfo
	 * @return the sequence of vertices on a shortest path, as an Iterable
	 * @throws IllegalArgumentException
	 *             unless {@code 0 <= v < V}
	 */
	public Iterable<VerticeInfo> pathTo(VerticeInfo v) {
		if (v == null || !hasPathTo(v))
			return null;
		Queue<VerticeInfo> path = new Queue<VerticeInfo>();
		VerticeInfo x = v;
		int dis = -1;
		if(x != null){
			path.enqueue(x);
			while (dis != 1) {
				dis = disTo.get( x.getId());
				x = edgeTo.get( x.getId());

				path.enqueue(x);
				return path;
			}
		}
		
		return null;

		
	}

	/**
	 * Returns the number of edges in a shortest path between the source
	 * VerticeInfo {@code s} (or sources) and VerticeInfo {@code v}?
	 * 
	 * @param v
	 *            the VerticeInfo
	 * @return the number of edges in a shortest path
	 * @throws IllegalArgumentException
	 *             unless {@code 0 <= v < V}
	 */
	public int distTo(VerticeInfo v) {
		return disTo.get((int) v.getId());
	}
}