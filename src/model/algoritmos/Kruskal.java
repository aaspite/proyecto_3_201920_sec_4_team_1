package model.algoritmos;

import java.util.Iterator;


import model.data_structures.MinPQ;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHash;
import model.logic.Edge;
import model.logic.EdgeWeightedGraph;
import model.logic.VerticeInfo;

public class Kruskal {
	private static final double FLOATING_POINT_EPSILON = 1E-12; 

	private double weight; 
	private Queue<Edge> mst ; 
	private SeparateChainingHash<Integer, Integer > vertices;
	private Long IntegerToLong [];

	/**
	 * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
	 * @param G the edge-weighted graph
	 */
	public Kruskal() {
		
	}
	

	public Queue<Edge> getMst(EdgeWeightedGraph<String, VerticeInfo> G) {
		mst = new Queue<>();
		MinPQ<Edge> pq = new MinPQ<>(); 
		
		
		//Iterar sobre G
		
		int i = 0;

		while (i < G.V()) {
			//VerticeInfo actual = (VerticeInfo) G.getInfoVertex(Integer.toString(i));

				Iterable<Edge> edges = G.adj(i);
				if(edges != null){
					Iterator<Edge> iterador = edges.iterator();
					while(iterador.hasNext()){
						Edge actual = iterador.next();
						pq.insert(actual);
					}
				}

			i++;
		}
		
		/*
		Iterator<Queue<Edge>> it = G.getMyAdjList().values().iterator(); 
		longToInteger =  new SeparateChainingHash<Long,Integer > ();
		IntegerToLong = new Long [G.E()];
		
		while(it.hasNext()) {
			Queue<Edge> ed = it.next(); 
			for (Edge e : ed)
				pq.insert(e);
		}
		*/

		UF uf = new UF(G.V()); 
		while(!pq.isEmpty() && mst.size() < G.V() -1) {
			Edge e = pq.delMax(); 
			VerticeInfo v = G.getInfoVertex(Integer.toString(e.either()));
			VerticeInfo w = G.getInfoVertex(Integer.toString(e.other(e.either()))); 
			
			vertices =  new SeparateChainingHash<Integer,Integer > ();
			if(v != null){
				if ( !vertices.contains(v.getId()))
				{
					//IntegerToLong[longToInteger.size()]=(Long)v.getId();
					vertices.put(v.getId(), vertices.size());
				}
				if(w != null){
					if ( !vertices.contains(w.getId()))
					{
						//IntegerToLong[longToInteger.size()]=(Long)w.getId();
						vertices.put(w.getId(), vertices.size());
					}
					if(!uf.connected(vertices.get(v.getId()),vertices.get(w.getId()))) {
						uf.union(vertices.get(v.getId()),vertices.get( w.getId()));
						
						mst.enqueue(e);
						weight+= e.weight();
					}
				}
				
			}
		}
		return mst;
	}

	public double weight() {
		return weight; 
	}

}