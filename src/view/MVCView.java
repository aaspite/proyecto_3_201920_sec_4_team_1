package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("Estructuras de Datos: Proyecto 3");
			System.out.println("Por Antonio Aspite y Mateo Perez");
			System.out.println("----------------------------------------------");
			System.out.println("-------------------MENU:----------------------");
			System.out.println("Parte Inicial:");
			System.out.println("1. Cargar grafo ( y agregar informacion de costo);");
			System.out.println("2. Encontrar el Id del Vértice de la malla vial más cercano");
			System.out.println("PARTE A:");
			System.out.println("3. Encontrar el camino de costo mínimo");
			System.out.println("4. Determinar los n vértices con menor velocidad promedio");
			System.out.println("5. Calcular un árbol de expansión mínima (MST)");
			System.out.println("PARTE B:");
			System.out.println("6. Encontrar el camino de menor costo (menor distancia Haversine) para un viaje entre dos localizaciones geográficas");
			System.out.println("7. Indicar vértices alcanzables para un tiempo T");
			System.out.println("8. Calcular árbol de expansión mínima (MST) con criterio distancia y algoritmo de Kruskal");
			System.out.println("PARTE C:");
			System.out.println("9. Construir grafo simplificado No dirigido con vértices únicos");
			System.out.println("10. Calcular el camino de costo mínimo (algoritmo de Dijkstra)");
			System.out.println("11. Obtener caminos de menor longitud");
			System.out.println("12. Salir");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
