package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
	
	/**
	 *  metodo Run
	 */
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("Empezar a cargar");
				modelo.cargar(); 	
				System.out.println("Datos cargados :)");
				break;

			case 2:
				System.out.println("Ingrese la latitud: ");
				double latitud = lector.nextDouble();
				System.out.println("Ingrese la longitud: ");
				double longitud = lector.nextDouble();
				
				modelo.encontrarIdVerticeCercano(latitud, longitud);
				
				break;
			case 3:
				System.out.println("Ingrese la latitud de origen: ");
				double latitudOrigen = lector.nextDouble();
				System.out.println("Ingrese el longitud de origen: ");
				double longitudOrigen = lector.nextDouble();
				System.out.println("Ingrese el latitud de destino: ");
				double latitudDestino = lector.nextDouble();
				System.out.println("Ingrese el longitud de destino: ");
				double longitudDestino = lector.nextDouble();
				modelo.encontrarCaminoCostoMinimo(latitudOrigen, longitudOrigen, latitudDestino, longitudDestino);
				break;
			case 4:
				System.out.println("Ingrese N: ");
				int n = lector.nextInt();
				modelo.determinarNVerticesConMenorVelocidad(n);
				break;
			case 5:
				modelo.calcularMSTPrim();
				break;
			case 6:
				System.out.println("Ingrese la latitud de origen: ");
				double latitudOrigen7 = lector.nextDouble();
				System.out.println("Ingrese el longitud de origen: ");
				double longitudOrigen7 = lector.nextDouble();
				System.out.println("Ingrese el latitud de destino: ");
				double latitudDestino7 = lector.nextDouble();
				System.out.println("Ingrese el longitud de destino: ");
				double longitudDestino7 = lector.nextDouble();
				modelo.encontrarCaminoMenorCosto(latitudOrigen7, longitudOrigen7, latitudDestino7, longitudDestino7);
				break;
			case 7:
				System.out.println("Ingrese el tiempo T: ");
				int t = lector.nextInt();

				modelo.encontrarVerticesAlcanzables(t);
				break;
			case 8:
				modelo.calcularMSTKruskal();
				break;
			case 9:
				modelo.construirGrafoSimplificado();
				break;
			case 10: 
				System.out.println("Ingrese la zona de origen: ");
				int zonaOrigen = lector.nextInt();
				System.out.println("Ingrese la zona de destino: ");
				int zonaDestino = lector.nextInt();
				modelo.calcularCaminoCostoMinimo(zonaOrigen, zonaDestino);
				break;
			case 11: 
				System.out.println("Ingrese la zona de origen: ");
				int zonaOrigen12 = lector.nextInt();
				modelo.calcularCaminosMenorLongitud(zonaOrigen12);
				break;	
				
			case 12: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;	

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
